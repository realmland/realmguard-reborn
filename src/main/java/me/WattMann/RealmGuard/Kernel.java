package me.WattMann.RealmGuard;

import me.WattMann.RealmGuard.discord.RealmGuard;
import me.WattMann.RealmGuard.log.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Kernel
{
    private static RealmGuard client;
    private static Properties props;

    public static void main(String ... args) {
        try {
            props = new Properties();
            props.load(new FileReader("launch.properties"));

            client = new RealmGuard(args[0], props);
        } catch (IOException e) {
            Logger.error(e, "Failed to load launch properties!");
        }
    }
}
