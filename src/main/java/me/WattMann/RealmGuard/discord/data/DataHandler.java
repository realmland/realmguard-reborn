package me.WattMann.RealmGuard.discord.data;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import me.WattMann.RealmGuard.discord.RealmGuard;
import me.WattMann.RealmGuard.discord.imp.RealmObject;
import me.WattMann.RealmGuard.discord.imp.Service;
import me.WattMann.RealmGuard.log.Logger;
import org.jetbrains.annotations.NotNull;

@Getter public class DataHandler implements RealmObject, Service
{
    private HikariConfig config;
    private HikariDataSource dataSource;
    private RealmGuard realmGuard;

    public DataHandler(@NotNull RealmGuard client) {
        realmGuard = client;
        config = new HikariConfig();
        config.setPoolName("DataPool");
        config.setDataSourceClassName(client.getProperties().getProperty("db_driver", "org.sqlite.SQLiteDataSource"));
        config.setJdbcUrl("jdbc:sqlite:" + client.getProperties().getProperty("data_storage", "data.db"));
    }

    @Override
    public RealmGuard getInstance() {
        return realmGuard;
    }

    @Override
    public void initialize() throws Exception {
        Logger.info("Connecting to Data Source!");
        dataSource = new HikariDataSource(config);
        Logger.info("Initialized DataHandler!");
    }

    @Override
    public void terminate() throws Exception {
        Logger.info("Closing connections...");
        dataSource.close();
        Logger.info("Terminated DataHandler!");
    }
}
