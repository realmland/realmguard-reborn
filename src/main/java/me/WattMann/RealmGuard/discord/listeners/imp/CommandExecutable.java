package me.WattMann.RealmGuard.discord.listeners.imp;

import jdk.nashorn.internal.objects.annotations.Getter;
import me.WattMann.RealmGuard.discord.RealmGuard;
import me.WattMann.RealmGuard.discord.listeners.model.CommandRaw;
import org.jetbrains.annotations.NotNull;

import java.util.function.Predicate;

public interface CommandExecutable extends Predicate<String>
{
    @NotNull @Getter String getLabel();
    void execute(@NotNull CommandRaw raw, @NotNull RealmGuard instance);
}
