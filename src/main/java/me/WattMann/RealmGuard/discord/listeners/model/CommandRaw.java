package me.WattMann.RealmGuard.discord.listeners.model;

import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.NumberFormat;
import java.text.ParseException;

public class CommandRaw
{
    @Nullable @Getter private Member sender;
    @NotNull @Getter private String label;
    @Nullable @Getter private Argument<?>[] arguments;


    public CommandRaw(@Nullable Member sender, @NotNull String label, @Nullable Argument<?>[] arguments) {
        this.sender = sender;
        this.label = label;
        this.arguments = arguments;
    }

    public static class Argument<Type>
    {
        @NotNull @Getter private Type value;

        public Argument(@NotNull Type value) {
            this.value = value;
        }

        public static Argument<?> from(@NotNull String arg) {
            try {
                Number num = NumberFormat.getInstance().parse(arg);
                return new Argument<>(num);
            } catch (ParseException ignored) {}

            return new Argument<>(arg);
        }
    }
}
