package me.WattMann.RealmGuard.discord.listeners;

import discord4j.core.event.domain.message.MessageCreateEvent;
import me.WattMann.RealmGuard.discord.RealmGuard;
import me.WattMann.RealmGuard.discord.imp.RealmObject;
import me.WattMann.RealmGuard.discord.listeners.imp.CommandExecutable;
import me.WattMann.RealmGuard.discord.listeners.model.CommandRaw;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashSet;

public class CommandRegistry implements RealmObject
{
    @NotNull private RealmGuard instance;
    @NotNull private HashSet<CommandExecutable> executables = new HashSet<>();

    public CommandRegistry(@NotNull RealmGuard instance) {
        this.instance = instance;
        instance.getDiscordClient().getEventDispatcher().on(MessageCreateEvent.class).subscribe(this::onMessage);
    }

    private void onMessage(@NotNull MessageCreateEvent event) {
        event.getMessage().getContent().ifPresent(message -> {
           if(message.startsWith(getInstance().getProperties().getProperty("command_prefix", "rm"))) {
               String[] args = message.split(" ");
               if(args.length < 2) {
                   //error
                   return;
               }
               String label = args[1];
               args = (String[]) Arrays.stream(args).skip(2).toArray(); //skip prefix and label

               CommandRaw.Argument<?>[] arguments = new CommandRaw.Argument[args.length];
               for(int index = 0; index < args.length; index++) {
                   arguments[index] = CommandRaw.Argument.from(args[index]);
               }
               CommandRaw raw = new CommandRaw(event.getMember().orElse(null), label, arguments);


               for (CommandExecutable executable : executables) {
                   if(executable.getLabel().equalsIgnoreCase(label)) {
                       executable.execute(raw, instance);
                   }
               }
           }
        });
    }

    @Override
    public @NotNull RealmGuard getInstance() {
        return instance;
    }
}
