package me.WattMann.RealmGuard.discord.listeners;

import discord4j.core.event.EventDispatcher;
import discord4j.core.event.domain.guild.MemberJoinEvent;
import discord4j.core.object.entity.MessageChannel;
import discord4j.core.object.util.Snowflake;
import me.WattMann.RealmGuard.discord.RealmGuard;
import me.WattMann.RealmGuard.discord.imp.RealmObject;
import me.WattMann.RealmGuard.discord.imp.Service;
import me.WattMann.RealmGuard.log.Logger;
import org.jetbrains.annotations.NotNull;

public class EventHandlers implements RealmObject, Service {
    @NotNull private RealmGuard guard;

    public EventHandlers(@NotNull RealmGuard guard) {
        this.guard = guard;
    }


    @Override
    public @NotNull RealmGuard getInstance() {
        return guard;
    }

    @Override
    public void initialize() throws Exception {
        EventDispatcher dispatcher = guard.getDiscordClient().getEventDispatcher();
        if (getInstance().getProperties().containsKey("welcome_channel") && getInstance().getProperties().containsKey("welcome_message")) {
            dispatcher.on(MemberJoinEvent.class).subscribe(memberJoinEvent -> {

                memberJoinEvent.getGuild().subscribe(guild -> {
                    if(getInstance().getProperties().containsKey("member_role"))
                        memberJoinEvent.getMember().addRole(Snowflake.of(getInstance().getProperties().getProperty("member_role"))).log().subscribe();
                    guild.getChannelById(Snowflake.of(getInstance().getProperties().getProperty("welcome_channel"))).subscribe(channel -> {
                        ((MessageChannel) channel).createMessage(String.format(getInstance().getProperties().getProperty("welcome_message"), memberJoinEvent.getMember().getMention())).log().subscribe();
                    });
                });
            });
            Logger.info("Registered welcome message handler!");
        }



        Logger.info("Event Handlers initialized!");
    }

    @Override
    public void terminate() throws Exception {

    }
}
