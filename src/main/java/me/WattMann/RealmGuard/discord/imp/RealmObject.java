package me.WattMann.RealmGuard.discord.imp;

import me.WattMann.RealmGuard.discord.RealmGuard;
import org.jetbrains.annotations.NotNull;

public interface RealmObject
{
    @NotNull RealmGuard getInstance();
}
