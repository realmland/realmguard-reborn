package me.WattMann.RealmGuard.discord.imp;

public interface Service
{
    void initialize() throws Exception;
    void terminate() throws Exception;

    default void reload() throws Exception {
        terminate();
        initialize();
    }
}
