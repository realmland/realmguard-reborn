package me.WattMann.RealmGuard.discord;

import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.domain.guild.MemberJoinEvent;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import lombok.Getter;
import me.WattMann.RealmGuard.discord.data.DataHandler;
import me.WattMann.RealmGuard.discord.imp.RealmObject;
import me.WattMann.RealmGuard.discord.listeners.EventHandlers;
import me.WattMann.RealmGuard.log.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.Properties;

@Getter public class RealmGuard implements RealmObject {

    private DiscordClient discordClient;
    private DataHandler dataHandler;
    private Properties properties;
    private EventHandlers handlers;

    public RealmGuard(@NotNull String secret,@NotNull Properties properties) {
        this.properties = properties;
        dataHandler = new DataHandler(this);
        handlers = new EventHandlers(this);
        discordClient = new DiscordClientBuilder(secret).build();

        try {
            // dataHandler.initialize();
        } catch (Exception e) {
            Logger.error(e, "Failed to initialize DataHandler!");
            return;
        }

        try {
            handlers.initialize();
        } catch (Exception e) {
            Logger.error(e, "Failed to initialize EventHandlers!");
            e.printStackTrace();
            return;
        }

        hook();

        discordClient.login().block();
    }

    private void hook() {
        discordClient.getEventDispatcher().on(ReadyEvent.class).subscribe((event) -> {
            Logger.info("Lock and load!");
        });

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            Logger.info("Terminating...");
            try {
                discordClient.logout().subscribe((param) -> Logger.info("BAI BAI BEJBY BLU"));
                dataHandler.terminate();
                Logger.info("DataHandler terminated!");
            } catch (Exception e) {
                Logger.error(e, "Failed to terminate DataHandler!");
            }
        }));

        Logger.info("All hooks have been initialized!");
    }

    @Override
    public @NotNull RealmGuard getInstance() {
        return this;
    }
}
