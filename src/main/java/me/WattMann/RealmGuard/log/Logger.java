package me.WattMann.RealmGuard.log;

public class Logger
{

    private static void write(String msg) {
        System.out.println("# " + msg);
    }
    public static void info(String msg, Object ... format) {
        write(String.format(msg, format));
    }
    public static void warn(String msg, Object ... format) {
        write("WARN :: " + String.format(msg, format));
    }
    public static void error(String msg, Object ... format) {
        write("ERROR :: " + String.format(msg, format));
    }
    public static void error(Throwable t, String msg, Object ... format) {
        write("ERROR :: " + String.format(msg, format) + " caused by: " + t.toString());
    }
}
